
let currencyLabel = "UAH";
const sumSlider = document.getElementById('sum');
const sumText = document.getElementById('sumValue');
const termSlider = document.getElementById('term');
const termText = document.getElementById('termValue');
const percent = document.getElementById('percent');
const result = document.getElementById('result');
const invest = document.getElementById('investments');
const sumMax = document.getElementById('sum-max');
const termMax = document.getElementById('term-max');


const throwValues = () => {
    sumSlider.value = 0;
    sumText.value = 0;
    termSlider.value = 0;
    termText.value = 0;
    percent.value = 0;
    invest.innerHTML = `0 ${currencyLabel}`;
    result.textContent = `0 ${currencyLabel}`;
}

const createSelectOptions = (currency) => {

    percent.innerHTML = "";

    switch(currency) {
        case "UAH": 
            for (let i=1; i <= 20; i++) {
                percent.innerHTML += `<option>${i}</option>\n`;
            }
        break;

        case "USD": 
            for (let i=1; i <= 12; i++) {
                percent.innerHTML += `<option>${i}</option>\n`;
            }
        break;

        case "EUR": 
            for (let i=1; i <= 10; i++) {
                percent.innerHTML += `<option>${i}</option>\n`;
            }
        break;
    }
}

const сurrencyHandler = () => {

    const UAH = document.getElementById('UAH');
    const USD = document.getElementById('USD');
    const EUR = document.getElementById('EUR');



    UAH.onclick = (e) => {
        currencyLabel = "грн";
        e.target.checked = "";
        e.target.className = "btn btn-primary active";
        EUR.className = "btn btn-primary";
        USD.className = "btn btn-primary";
        sumSlider.max = 10000000;
        sumMax.innerText = sumSlider.max;
        createSelectOptions("UAH");
        throwValues();
    }
    
    USD.onclick = (e) => {
        currencyLabel = "$";
        e.target.checked = "";
        e.target.className = "btn btn-primary active";
        EUR.className = "btn btn-primary";
        UAH.className = "btn btn-primary";
        sum.max = 500000;
        sumMax.innerText = sumSlider.max;
        createSelectOptions("USD");
        throwValues();
    }

    EUR.onclick = (e) => {
        currencyLabel = "€";
        e.target.checked = "";
        e.target.className = "btn btn-primary active";
        USD.className = "btn btn-primary";
        UAH.className = "btn btn-primary";
        sum.max = 400000;
        sumMax.innerText = sumSlider.max;
        createSelectOptions("EUR");
        throwValues();
    }
}

const modeHandler = () => {

    const creditLink = document.getElementById('credit-link');
    const depositLink = document.getElementById('deposit-link');
    const header = document.getElementById('header-legend');
    const accPeriod = document.getElementById('acc-period');

    creditLink.onclick = (e) => {
        e.target.className = "nav-link active";
        depositLink.className = "nav-link";
        header.innerHTML = "Кредитний калькулятор";  
        termSlider.max = 10;
        termMax.innerText = termSlider.max;
        accPeriod.style.display = 'none';
        throwValues();
    }
    
    depositLink.onclick = (e) => {
        e.target.className = "nav-link active";
        creditLink.className = "nav-link";  
        header.innerHTML = "Депозитний калькулятор";
        termSlider.max = 20;
        termMax.innerText = termSlider.max;
        accPeriod.style.display = 'block';
        throwValues();
    }
}


const sliderHandler = (sliderId) => {
    let slider = document.getElementById(sliderId);
    let text = document.getElementById(`${sliderId}Value`);

    slider.oninput = (() => {
      let value = slider.value;
      text.value = value;
    });

    text.oninput = (() => {
        let value = text.value;
        slider.value = value;
    })
  }

const calculate = () => {

    const button = document.getElementById('calculate');    
    const yearRadio = document.getElementById('year');
    const halfYearRadio = document.getElementById('half-year');
    const quarterRadio = document.getElementById('quarter');
    const monthRadio = document.getElementById('month');

    button.onclick = (e) => {

        const info = `<sub id="investments-info">(${termSlider.value} років, під ${percent.value}% річних)<sub>`;
        const period = yearRadio.checked ? 1: halfYearRadio.checked ? 2: quarterRadio.checked ? 4: monthRadio.checked ? 12: 1; 
        const formula = Math.ceil(sumSlider.value * Math.pow( (1 + percent.value/100/period), termSlider.value*period ));
        

        invest.innerHTML = ` ${sumSlider.value} ${currencyLabel} ${info} `;
        result.textContent = `${formula} ${currencyLabel}`;
    }
}


